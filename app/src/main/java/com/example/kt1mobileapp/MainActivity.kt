package com.example.kt1mobileapp

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.util.Log.d
import android.util.TypedValue
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.PackageManagerCompat.LOG_TAG
import androidx.preference.PreferenceManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext
import org.json.JSONObject
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL


class MainActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    var sunSign = "Aries"
    var resultTitleView: TextView? = null
    var resultContentView: TextView? = null
    var welcomeTextView: TextView? = null

    lateinit var sharedPreferences: SharedPreferences;
    lateinit var editor: SharedPreferences.Editor;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(baseContext);
        val switchPref: Boolean = sharedPreferences.getBoolean("signSwitch", false);
        val largeText: Boolean = sharedPreferences.getBoolean("largeFont", false);
        val darkTheme: Boolean = sharedPreferences.getBoolean("darkTheme", false);
        if(darkTheme) {
            setTheme(R.style.DarkTheme)
        } else {
            setTheme(R.style.LightTheme)
        }
        setContentView(R.layout.activity_main)
        var buttonView: Button = findViewById(R.id.button)
        editor = sharedPreferences.edit();
        var value = "Aries";
        if(switchPref) {
            value = sharedPreferences.getString("sign", "Aries").toString()
        }

        buttonView.setOnClickListener {
            GlobalScope.async {
                getPredictions(buttonView)
            }
        }
        val spinner = findViewById<Spinner>(R.id.spinner)
        val adapter = ArrayAdapter.createFromResource(this,R.array.sunsigns,android.R.layout.simple_spinner_item)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        val spinnerPosition: Int = adapter.getPosition(value)
        spinner.adapter = adapter;
        spinner.onItemSelectedListener = this
        spinner.setSelection(spinnerPosition);
        resultTitleView = findViewById(R.id.resultTitle)
        resultContentView = findViewById(R.id.resultContent)
        welcomeTextView = findViewById(R.id.textView2);

        if(largeText) {
            (resultContentView as TextView).setTextSize(TypedValue.COMPLEX_UNIT_SP, 24f);
            (resultTitleView as TextView).setTextSize(TypedValue.COMPLEX_UNIT_SP, 28f);
            (welcomeTextView as TextView).setTextSize(TypedValue.COMPLEX_UNIT_SP, 42f);
        }

    }


    public suspend fun getPredictions(view: android.view.View) {
        editor.putString("sign", sunSign.toString())
        editor.apply()
        try {
            val result = withContext(Dispatchers.Default) {
                callAztroAPI("https://sameer-kumar-aztro-v1.p.rapidapi.com/?sign=" + sunSign.toString() + "&day=today")
            }

            onResponse(result)

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun callAztroAPI(apiUrl:String ):String?{
        var result: String? = ""
        val url: URL
        var connection: HttpURLConnection? = null
        try {
            url = URL(apiUrl)
            connection = url.openConnection() as HttpURLConnection
            // set headers for the request
            // set host name
            connection.setRequestProperty("x-rapidapi-host", "sameer-kumar-aztro-v1.p.rapidapi.com")

            // set the rapid-api key
            connection.setRequestProperty("x-rapidapi-key", "ea735e8286msh3287c5996cf7a0fp1815ffjsn350ad9073e2a")
            connection.setRequestProperty("content-type", "application/x-www-form-urlencoded")
            // set the request method - POST
            connection.requestMethod = "POST"
            val `in` = connection.inputStream
            val reader = InputStreamReader(`in`)

            // read the response data
            var data = reader.read()
            while (data != -1) {
                val current = data.toChar()
                result += current
                data = reader.read()
            }
            return result
        } catch (e: Exception) {
            e.printStackTrace()
        }

        // if not able to retrieve data return null
        return null

    }

    private fun onResponse(result: String?) {
        try {

            // convert the string to JSON object for better reading
            val resultJson = JSONObject(result)

            // Initialize prediction text
            var predictionTitle ="Today's prediction for "
            predictionTitle += this.sunSign+""

            // Update text with various fields from response
            var prediction = resultJson.getString("date_range")+"\n\n"
            prediction += resultJson.getString("description")

            //Update the prediction to the view
            setText(this.resultTitleView,predictionTitle)
            setText(this.resultContentView, prediction)

        } catch (e: Exception) {
            e.printStackTrace()
            setText(this.resultTitleView, "Oops!! Something went wrong")
            setText(this.resultContentView, "Start are silent right now :( Please, try again")
        }
    }

    private fun setText(text: TextView?, value: String) {
        runOnUiThread { text!!.text = value }
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        if (p0 != null) {
            sunSign = p0.getItemAtPosition(p2).toString()
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        sunSign = "Aries"
    }

    fun launchSecondActivity(view: View) {
        val intent = Intent(this, SettingsActivity::class.java);
        startActivity(intent);
    }
}